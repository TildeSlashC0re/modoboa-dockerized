#!/bin/bash

cd deps

cd modoboa-postgres-alpine
docker build -t c0re/modoboa-postgres-alpine .

cd ..
cd modoboa-dovecot-alpine
docker build -t c0re/modoboa-dovecot-alpine .

cd ..
cd modoboa-postfix-alpine
docker build -t c0re/modoboa-postfix-alpine . 

cd .. 
cd modoboa-amavis-alpine-s6
docker build -t c0re/modoboa-amavis-alpine -f Dockerfile.finalize . 

cd ..
cd modoboa 
docker build -t c0re/modoboa -f Dockerfile.fullest . 
